(function() {
	'use strict';
	/**
	 * Controller to manage the Exponential smoothing.
	 * https://www.predix-ui.com/#/components/px-vis-timeseries/
	 *
	 */
	function PxVistimeseriesCtrl($scope, $timeout, $element, $q, $http) {
		var ctrl = this,
			Timeseries = $('px-vis-timeseries', $element),
			DECISYON_DATA = $scope.DECISYON.target.content.data,
			ctx = $scope.DECISYON.target.content.ctx,
			predixData = $scope.DECISYON.target.content.ctx.predixIntegration.value,
            uaaAccessToken = predixData.uaaAccessToken,
            zoneId = predixData.zoneId,
			getTarget = $scope.DECISYON.target.page,
			target = $scope.DECISYON.target,
			mshPage = target.page,
			refObjId = ctx.instanceReference.value,
			baseName = ctx.baseName.value,
			DEFAULT_METHOD = 'DE',
			DEFAULT_FORCASTVAL = '7',
			EMPTY_PARAM_VALUE = 'PARAM_VALUE_NOT_FOUND',
			OVERFLOW_WIDTH_BUG_FIX = 50,
			OVERFLOW_HEIGHT_BUG_FIX = 35,
			unbindWatches = [];

		/* Return the size input after adjusting its value by removing % and space white.
		The width and the height are adjusted because the Predix component doesn't supported values in percentage*/

		/* It adjust size ad fix overflow*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace(/%/g, '').trim();
			return (parseInt(adjustedSize) !== '') ?	adjustedSize	:	'';
		};

		/* 	Set the container widget size.If you set both this and height to "auto", the chart will expand to fill its containing element.
			Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		*/

		/* widget displays two components on a page and the height within the px-vis-timeseries is for the first component.
		Thus if we have height as 400 in the mashboard then in that case to 400px will be the container height 
		And the proportal value for the first componenet will be calculated with below function
		This will work for the value 350 and above in mashboard.
		*/
		var getHeightProportionalPercentage = function(propheight) {
			return ((55 / 100) * propheight);
		};
		var applyDimensionsToWebComponent = function(ctx) {
			var height	=	getAdjustedSizing(ctx['min-height'].value),
				width = getAdjustedSizing(ctx['min-width'].value);
			ctrl.height	=	(height !== '' && height	>	getHeightProportionalPercentage(height))	?	(height - getHeightProportionalPercentage(height)) : height;
			ctrl.width	=	(width !== '' && width	>	OVERFLOW_WIDTH_BUG_FIX)	&& width > 100 ?	(width - OVERFLOW_WIDTH_BUG_FIX) : width;
			ctrl.contHeight	= (height !== '' && height	>	OVERFLOW_HEIGHT_BUG_FIX)	?	(height - OVERFLOW_HEIGHT_BUG_FIX) : height; // for the widget container
		};
		/* to remove the watch if the widget is not on page*/
		var destroyWidget = function() {			
			for (var i = 0; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};

		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			$scope.$watch('DECISYON.target.content', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)) {
					ctrl.manageProperty(newCtx.ctx);
					ctrl.exponentialServiceCall();
				}
			}, true);
			//Listener on destroy angular element; in this case we destroy all watch and listener
			$scope.$on('$destroy', destroyWidget);
		};

		/* Function to handle DC failure */
		ctrl.setError = function(errorMsg) {
			ctrl.errorMsg = errorMsg;
			ctrl.currentTpl = 'error.html';
			destroyWidget();
		};

		/* Managing of the custom settings from context */
		ctrl.manageProperty = function(content) {
			ctrl.num_forcast = (content.$numForecast.value !== '') ? content.$numForecast.value : DEFAULT_FORCASTVAL ;
			ctrl.method = ( (content.$method.value !== '') && (!angular.equals(content.$method.value.value,EMPTY_PARAM_VALUE))) ? content.$method.value.value : DEFAULT_METHOD ;
			ctrl.widgetID = 'pxvistimeseries_' + refObjId;
			ctrl.history = content.$historyDays.value;
			ctrl.tagName = content.$tagName.value;
			ctrl.ExponentialJson.params.num_forecast = ctrl.num_forcast ;    //this Json contain the format in which we will query the exponential smooting service
			ctrl.ExponentialJson.params.method = ctrl.method ;
		};

		/* Spinner functionality*/
		ctrl.spinner = function(state) {
			var elementSpinnerContainer = document.getElementById(ctrl.widgetID).parentNode,
				spinner = elementSpinnerContainer.querySelector('px-spinner'),
				overlay = elementSpinnerContainer.querySelector('#px-fade');
			if (state === 'show') {
				spinner.removeAttribute('finished', true);
				overlay.style.display = 'block';
				overlay.style.width = (parseInt(ctrl.width)) + 'px';
			} else if (state === 'hide') {
				spinner.setAttribute('finished', true);
				overlay.style.display = 'none';
			}
		};
        /*this function will return the promise for the timeseries call */
		ctrl.getPromiseFromTimeseries = function(token) {
			var deferer = $q.defer();
			ctrl.spinner('show');
			$http({
				method: 'POST',
				url: 'https://time-series-store-predix.run.aws-usw02-pr.ice.predix.io/v1/datapoints',
				headers: {
					'authorization': 'Bearer ' + token,
					'predix-zone-id': zoneId,
					'content-type': 'application/json'
				},
				data: {
					'start': ctrl.history+'d-ago',
					'tags': [{
						'name': ctrl.tagName
					}]
				}
			}).then(function successCallback(response) {
				deferer.resolve(response);
			}, function errorCallback(response) {
				deferer.reject(response);
			});
			return deferer.promise;
		};

		/* funciton which will make Timeseries ajax call */
		ctrl.timeSeriesServiceCall = function() {
			ctrl.getPromiseFromTimeseries(uaaAccessToken).then(function(results) {
				var i = 0,
					difference = 0;
				ctrl.spinner('hide');
				ctrl.predictVal = new Array([]);
				ctrl.JsonArr = new Array([]);
				angular.forEach(results.data.tags[0].results[0].values, function(value, key) {
					ctrl.JsonArr[key] = value[0];
					ctrl.ExponentialJson.time_series.push(value[1]);
					ctrl.chartData.push({
						'x': value[0],
						'y0': value[1],
						'y1': ''
					});
				});
				//Forecast data calculation
				difference = parseInt(ctrl.JsonArr[1]) - parseInt(ctrl.JsonArr[0]);
				for (i = 0; i < parseInt(ctrl.JsonArr.length); i++) {
					if (i > 0) {
						ctrl.predictVal[i] = parseInt(ctrl.predictVal[i - 1]) + parseInt(difference);
					} else if (i === 0) {
						ctrl.predictVal[i] = parseInt(ctrl.JsonArr[parseInt(ctrl.JsonArr.length) - 1]);
					}
				}
				if (ctrl.ExponentialJson.time_series.length>0) {
					ctrl.exponentialServiceCall();
				}
			},
			function errorCallback(response) {
				ctrl.setError(response.data.errors[0]);
			});
		};

		/* Exponential Service ajax call*/
		ctrl.exponentialServiceCall = function() {
			ctrl.exponentialPromiseCall().then(function(response) {
				var predictedResultFromService,
					j,
					predictedValue = new Array([]);
				ctrl.spinner('hide');
				predictedResultFromService = angular.fromJson(response.data.result);
				angular.forEach(predictedResultFromService, function(value, key) {
					predictedValue[key] = value;
				});
				for (j = 0 ; j < ctrl.predictVal.length  ; j++) {
					ctrl.chartData.push({
						'x': ctrl.predictVal[j],
						'y0': '',
						'y1': predictedValue[j]
					});
				}
			});
		};
        
		/*this function will return the promise for the exponential call */
		ctrl.exponentialPromiseCall = function() {
			var deferer = $q.defer();
			ctrl.spinner('show');
			if (ctrl.ExponentialJson.time_series.length > 0) {
			    ctrl.ExponentialJson.params.period = ctrl.ExponentialJson.time_series.length;
				$http({
					method: 'POST',
					url: ENV_VARS.getContextPath() + '/HttpRequestForwarderServlet',
					headers: {
						'Predix-Zone-Id': '8c6c6760-1db3-4dce-80d9-a1d67bc2aadc',
						'Authorization': 'Bearer ' + uaaAccessToken,
						'Content-Type': 'application/json',
						'Dcy-Accepted-Headers': 'Predix-Zone-Id;Authorization;Content-Type'
					},
					params: {
						'targetUrl': 'https://predix-analytics-catalog-release.run.aws-usw02-pr.ice.predix.io/api/v1/catalog/analytics/3fc40745-c0e2-45e6-877f-30909876c54d/execution'
					},
					data: {
						'time_series': ctrl.ExponentialJson.time_series,
						'params': ctrl.ExponentialJson.params
					}
				}).then(
					function successCallback(response) {
						deferer.resolve(response);
					},
					function errorCallback(response) {
						deferer.reject(response);
						ctrl.spinner('hide');
					}
				);
			} else {
				ctrl.setError({errorMsg : 'Error : Data not found.'});
			}
			return deferer.promise;
		};

		/* Function will be fired once the polymer component are loaded. */
		ctrl.setExponentialLibraryLoaded = function() {
			ctrl.spinner('hide');
			applyDimensionsToWebComponent(ctx);
			ctrl.timeSeriesServiceCall();
		};

		/* Includes functionality corresponding to selected template */
		$scope.$on('$includeContentRequested', function(angularEvent, src) {
			if (src == 'Exponential.html') {
				$timeout(function() {
					ctrl.manageProperty(ctx);
					attachListeners();
				});
			}
		});

		/* Initialize */
		ctrl.inizialize = function() {
			ctrl.chartData = [];
			ctrl.ExponentialJson = {
				'time_series': [],
				'params': {
					'num_forecast' : '',
					'method' : '',
					'period' : ''
				}
			};
			ctrl.currentTpl = 'Exponential.html';
		};
		ctrl.inizialize();
	}
	PxVistimeseriesCtrl.$inject = ['$scope', '$timeout', '$element', '$q', '$http'];
	DECISYON.ng.register.controller('PxVistimeseriesCtrl', PxVistimeseriesCtrl);

}());